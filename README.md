
# About
This is an experimental hobby project that allows a student to learn the basics of music.  It is under semi-active development by a Computer Scientist, me, who knows pretty much nothing about music or music theory.  I'm adding things as I learn, and have made this public on the off chance that someone else finds it useful.


Just to reiterate the point, I'm not a teacher, I'm not your teacher, I'm not a music theory guy...  I wouldn't even classify myself as somewhat competent at music.  I'm just a guy who wanted a program to practice what I read.  I also wanted to keep my Java skills somewhat sharp, so this is a good way for me to force myself to use that language.


I'm using the tutorials at http://www.musictheory.net/lessons as a loose guide; if you actually want to learn something, you should definitely head over there.  I found them helpful, and they seem like they actually know what they are doing.  Even if you want to use this tutorial program, it (for now) points you at specific lessons to read before allowing you to practice what you've learned.

The main features of this program include:
*    Easy to use, menu driven UI
*    Accessible to screen reader and all other users
*    Practice basic concepts of music
*    Use Jfugue to actually hear music as you practice (future work)
*    Easy tutorial system that anyone can add to (I'll set up a guide on how to do that at some point)



