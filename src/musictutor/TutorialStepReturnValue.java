package musictutor;

public enum TutorialStepReturnValue {
MENU,
EXIT,
NEXT_STEP,
REPEAT
}
