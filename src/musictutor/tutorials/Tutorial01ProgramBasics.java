package musictutor.tutorials;

import musictutor.TutorialStepReturnValue;
import musictutor.annotations.Tutorial;
import musictutor.annotations.TutorialStep;
import musictutor.io.InputCommand;
import musictutor.io.InputOutputManager;

@Tutorial(title = "Introduction to the tutorial system", tutorialNumber = 1)
public class Tutorial01ProgramBasics {
@TutorialStep(title = "Introduction", stepNumber = 1)
	public TutorialStepReturnValue tutorialStep01(InputOutputManager ioManager) {
	String text = 
			"Music Tutor Program Basics\n" + 
			"\n" + 
			"\n" + 
			"This tutorial will teach you how to operate the Music Tutor program.  You don't need any prior knowledge to complete this tutorial.\n" + 
			"\n" + 
			"As you work through tutorials, you will see an explanation such as this one, and will be expected to respond using the keyboard.\n" +
			"Sometimes you will be asked to type something, and sometimes, like this one, you will just have to press enter.\n";
	ioManager.writeLine(text);
	
	prompt1loop: while(true) {
	InputCommand cmd = ioManager.readInput("Press enter to continue:");
	if(cmd == InputCommand.EXIT) { return TutorialStepReturnValue.EXIT; }
	else if(cmd == InputCommand.MENU) { return TutorialStepReturnValue.MENU; }
	else if(cmd == InputCommand.HELP) { ioManager.writeLine("You shouldn't know this command already, so I'm not going to give you any help."); continue; }
	
if(ioManager.getLastInputString().length() > 0) {
			ioManager.writeLine("I said press enter, not enter text and then press enter.  Most of the time, I'll ignore anything you type at a prompt like this one, but not this time!  Try this again...");
			continue;
		} else {
		break prompt1loop;
		}
	}
	return TutorialStepReturnValue.NEXT_STEP;
}

}
