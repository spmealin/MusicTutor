package musictutor.tutorials;

import java.util.Random;

import org.jfugue.parser.ParserException;

import musictutor.TutorialStepReturnValue;
import musictutor.annotations.Tutorial;
import musictutor.annotations.TutorialStep;
import musictutor.io.InputCommand;
import musictutor.io.InputOutputManager;
import musictutor.music.MusicParser;
import musictutor.music.NoteUtils;

@Tutorial(title = "Counting Steps", tutorialNumber = 2)
public class Tutorial02CountingSteps {
	
	@TutorialStep(title = "Intro", stepNumber = 1)
	public TutorialStepReturnValue tutorialStep01(InputOutputManager ioManager) {
		String intro = "This tutorial will practice counting up steps between notes.\r\n" + 
				"\r\n" + 
				"For reference, you should read:\r\n" + 
				"http://www.musictheory.net/lessons/20\r\n" + 
				"\r\n" + 
				"";
		ioManager.writeLine(intro);
		
		while(true) {
		InputCommand cmd = ioManager.readInput("Press enter to continue:");
		if(cmd == InputCommand.EXIT) { return TutorialStepReturnValue.EXIT; }
		else if(cmd == InputCommand.MENU) { return TutorialStepReturnValue.MENU; }
		else if(cmd == InputCommand.HELP) { ioManager.writeLine("Do you really need help to press enter?  Here's a hint: press enter."); continue; }
		break;
		}
		return TutorialStepReturnValue.NEXT_STEP;
	}

@TutorialStep(title = "Counting Up Between Natural Notes", stepNumber = 2)
public TutorialStepReturnValue tutorialStep02(InputOutputManager ioManager) {
	Random rand = new Random();
	MusicParser musicParser = new MusicParser();
	
	String intro = "For this part of the tutorial, you will be working with natural notes only.\r\n" + 
			"\r\n" + 
			"You will be given a starting note, such as 'C', and you will be asked to count up some number of steps, such as 4.  You must tell me what note that is, such as 'E' in this example.\r\n" + 
			"";
	ioManager.writeLine(intro);
	
	int count = 0;
	while(count < 10) {
		byte base = (byte)(rand.nextInt(NoteUtils.NUMBER_OF_NOTES) + 5*NoteUtils.NUMBER_OF_NOTES);  // Adding 5*NoteUtils.NUMBER_OF_NOTES=60 will put this in the 5th octive
		if(!NoteUtils.IsNaturalNote(base)) {
			continue;
		}
		byte steps = (byte)(rand.nextInt(NoteUtils.NUMBER_OF_NOTES)+1);
		byte answer = (byte)(base + steps);
		// force the answer to be in the 5th octive 
		if(answer >= 6*NoteUtils.NUMBER_OF_NOTES) {
			answer -= NoteUtils.NUMBER_OF_NOTES;
		}
		if(!NoteUtils.IsNaturalNote(answer)) {
			continue;
		}
		String prompt;
		if(steps == 1) {
			prompt = "What is " + steps + " step above " + NoteUtils.GetCanonicalNote(base) + ":";
		} else {
		prompt = "What is " + steps + " steps above " + NoteUtils.GetCanonicalNote(base) + ":";
		}
		
		input1loop: while(true) {
			InputCommand cmd = ioManager.readInput(prompt);
			if(cmd == InputCommand.EXIT) { return TutorialStepReturnValue.EXIT; }
			else if(cmd == InputCommand.MENU) { return TutorialStepReturnValue.MENU; }
			else if(cmd == InputCommand.HELP) { ioManager.writeLine("I should probably implement help at some point."); continue; }
			
			try {
				musicParser.ParseString(ioManager.getLastInputString());
			} catch(ParserException ex) {
				ioManager.writeLine("Please enter a valid music string.");
				continue;
			}
			
			if(musicParser.GetParsedElementCount() == 1 && musicParser.GetParsedValues()[0] == answer) {
				ioManager.writeLine("Correct!");
				count++;
				break input1loop;
			}
			ioManager.writeLine("That is not correct.");
		}
	}
	return TutorialStepReturnValue.NEXT_STEP;
}

@TutorialStep(title = "Counting Up Starting From Natural Notes", stepNumber = 3)
public TutorialStepReturnValue tutorialStep03(InputOutputManager ioManager) {
	Random rand = new Random();
	MusicParser musicParser = new MusicParser();
	
	String intro = "Now, you will have to answer with non-natural notes (sharp or flat) as well.\r\n" + 
			"\r\n" + 
			"Once again, you will be given a starting note, such as 'C', and you will be asked to count up some number of steps, such as 3.  You must tell me what note that is, such as 'D#' in this example.\r\n" +
			"Enharmonic notes are fine; so 'Eb' would have been a valid answer too.\r\n" +
			"";
	ioManager.writeLine(intro);
	
	int count = 0;
	while(count < 15) {
		byte base = (byte)(rand.nextInt(NoteUtils.NUMBER_OF_NOTES) + 5*NoteUtils.NUMBER_OF_NOTES);  // Adding 5*NoteUtils.NUMBER_OF_NOTES=60 will put this in the 5th octive
		if(!NoteUtils.IsNaturalNote(base)) {
			continue;
		}
		byte steps = (byte)(rand.nextInt(NoteUtils.NUMBER_OF_NOTES)+1);
		byte answer = (byte)(base + steps);
		// force the answer to be in the 5th octive 
		if(answer >= 6*NoteUtils.NUMBER_OF_NOTES) {
			answer -= NoteUtils.NUMBER_OF_NOTES;
		}
		String prompt;
		if(steps == 1) {
			prompt = "What is " + steps + " step above " + NoteUtils.GetCanonicalNote(base) + ":";
		} else {
		prompt = "What is " + steps + " steps above " + NoteUtils.GetCanonicalNote(base) + ":";
		}
		input1loop: while(true) {
			InputCommand cmd = ioManager.readInput(prompt);
			if(cmd == InputCommand.EXIT) { return TutorialStepReturnValue.EXIT; }
			else if(cmd == InputCommand.MENU) { return TutorialStepReturnValue.MENU; }
			else if(cmd == InputCommand.HELP) { ioManager.writeLine("I should probably implement help at some point."); continue; }
			
			try {
				musicParser.ParseString(ioManager.getLastInputString());
			} catch(ParserException ex) {
				ioManager.writeLine("Please enter a valid music string.");
				continue;
			}
			
			if(musicParser.GetParsedElementCount() == 1 && musicParser.GetParsedValues()[0] == answer) {
				ioManager.writeLine("Correct!");
				count++;
				break input1loop;
			}
			ioManager.writeLine("That is not correct.");
		}
	}
	return TutorialStepReturnValue.NEXT_STEP;
}

@TutorialStep(title = "Counting Up Between All Notes", stepNumber = 4)
public TutorialStepReturnValue tutorialStep04(InputOutputManager ioManager) {
	Random rand = new Random();
	MusicParser musicParser = new MusicParser();
	
	String intro = "Finally, all notes are in play (ha, see what I did there?).\r\n" + 
			"\r\n" + 
			"Once again, you will be given a starting note, such as 'C#', and you will be asked to count up some number of steps, such as 2.  You must tell me what note that is, such as 'D#' in this example.\r\n" +
			"Enharmonic notes are fine; so 'Eb' would have been a valid answer too.\r\n" +
			"";
	ioManager.writeLine(intro);
	
	int count = 0;
	while(count < 15) {
			byte base = (byte)(rand.nextInt(NoteUtils.NUMBER_OF_NOTES) + 5*NoteUtils.NUMBER_OF_NOTES);  // Adding 5*NoteUtils.NUMBER_OF_NOTES=60 will put this in the 5th octive
			byte steps = (byte)(rand.nextInt(NoteUtils.NUMBER_OF_NOTES)+1);
			byte answer = (byte)(base + steps);
			// force the answer to be in the 5th octive 
			if(answer >= 6*NoteUtils.NUMBER_OF_NOTES) {
				answer -= NoteUtils.NUMBER_OF_NOTES;
			}
		String prompt;
		if(steps == 1) {
			prompt = "What is " + steps + " step above " + NoteUtils.GetCanonicalNote(base) + ":";
		} else {
		prompt = "What is " + steps + " steps above " + NoteUtils.GetCanonicalNote(base) + ":";
		}
		input1loop: while(true) {
			InputCommand cmd = ioManager.readInput(prompt);
			if(cmd == InputCommand.EXIT) { return TutorialStepReturnValue.EXIT; }
			else if(cmd == InputCommand.MENU) { return TutorialStepReturnValue.MENU; }
			else if(cmd == InputCommand.HELP) { ioManager.writeLine("I should probably implement help at some point."); continue; }
			
			try {
				musicParser.ParseString(ioManager.getLastInputString());
			} catch(ParserException ex) {
				ioManager.writeLine("Please enter a valid music string.");
				continue;
			}
			
			if(musicParser.GetParsedElementCount() == 1 && musicParser.GetParsedValues()[0] == answer) {
				ioManager.writeLine("Correct!");
				count++;
				break input1loop;
			}
			ioManager.writeLine("That is not correct.");
		}
	}
	return TutorialStepReturnValue.NEXT_STEP;
}


}
