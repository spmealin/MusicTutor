package musictutor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.midi.MidiUnavailableException;

import org.atteo.classindex.ClassIndex;
import org.jfugue.parser.ParserException;
import org.jfugue.realtime.RealtimePlayer;

import musictutor.annotations.Tutorial;
import musictutor.annotations.TutorialStep;
import musictutor.io.InputCommand;
import musictutor.io.InputOutputManager;
import musictutor.music.MusicParser;

public class Tutor {
	private InputOutputManager ioManager;
	
	public Tutor() {
		ioManager = new InputOutputManager();
	}
	
	public void run() {
		ioManager.clearScreen();
		String banner = 
				"Music Tutor\n"
				+ "By Sean Mealin\n"
				+ "Version 0.1\n";
		ioManager.writeLine(banner);
		
		String menu = 
				"1.\tStart a tutorial\n"
				+ "2.\tPlay music\n"
						+ "\n0.\tExit program\n";
		ioManager.writeLine(menu);
		
		MainMenuLoop: while (true) {
InputCommand command = ioManager.readInput("Select an option:");
if(command == InputCommand.EXIT) {
	break MainMenuLoop;
}
			String input = ioManager.getLastInputString();
			switch (input) {
			case "1":
				selectTutorial();
			 ioManager.clearScreen();
			 ioManager.writeLine(banner);
			 ioManager.writeLine(menu);
				break;
			case "2":
				playMusic();
				ioManager.clearScreen();
				ioManager.writeLine(banner);
				 ioManager.writeLine(menu);
				break;
			case "0":
				break MainMenuLoop;
			default:
				ioManager.writeLine("Please select an option from the list above.");
				break;
			}
		}
		
		ioManager.writeLine("Good bye");
	}

	private void selectTutorial() {
List<Integer> tutorialNumbers = new ArrayList<>();
		Map<Integer, String> tutorialTitles = new HashMap<>();
		Map<Integer, Class<?>> tutorialClasses = new HashMap<>();
		
		for(Class<?> c : ClassIndex.getAnnotated(Tutorial.class)) {
		Tutorial tut = c.getAnnotation(Tutorial.class);
		if(tut != null) {
			tutorialNumbers.add(tut.tutorialNumber());
			tutorialTitles.put(tut.tutorialNumber(), tut.title());
			tutorialClasses.put(tut.tutorialNumber(), c);
		}
		}
		
		Collections.sort(tutorialNumbers);
		
		ioManager.clearScreen();
		String prompt =
				"Select a tutorial:\n\n";
		for(Integer i : tutorialNumbers) {
			prompt += i.toString() + ".\t";
			prompt += tutorialTitles.get(i) + "\n";
		}
		prompt += "\n0.\tExit\n";
		ioManager.writeLine(prompt);
		
		loopMenu: while(true) {
			InputCommand cmd = ioManager.readInput("Make a choice:");
			String temp = ioManager.getLastInputString();
			Integer choice = null;
			try {
			choice = Integer.parseInt(temp);
			} catch (Exception e) {
				// Do nothing
			}
			switch (cmd) {
			case EXIT:
				return;
			case OTHER:
				if (choice != null && tutorialNumbers.contains(choice)) {
					Class<?> c = tutorialClasses.get(choice);
					runTutorial(c);
					ioManager.clearScreen();
					ioManager.writeLine(prompt);
				} else if (choice != null && choice.intValue() == 0) {
					break loopMenu;
				}
				break;
			default:
				ioManager.writeLine("Please select an option from the list above.");
				break;
			}
		}
	}

	private void runTutorial(Class<?> tutorialClass) {
		List<Integer> stepNumbers = new ArrayList<>();
		Map<Integer, String> stepTitles = new HashMap<>();
		Map<Integer, Method> stepMethods = new HashMap<>();
		
		for(Method m : tutorialClass.getDeclaredMethods()) {
			if(!Modifier.isPublic(m.getModifiers())) {
// Ignore methods that are not public
				continue;
			}
			TutorialStep annotation = (TutorialStep)m.getAnnotation(TutorialStep.class);
			if(annotation == null) {
				// Not a tutorial step, skip it
				continue;
			}
stepNumbers.add(annotation.stepNumber());
stepTitles.put(annotation.stepNumber(), annotation.title());
stepMethods.put(annotation.stepNumber(), m);
		}
		Collections.sort(stepNumbers);
		
		String prompt = "Select a Tutorial Step:\n\n";
		for(Integer i : stepNumbers) {
			prompt += i.intValue() + ".\t" + stepTitles.get(i) + "\n";
		}
		prompt += "\n0.\tExit\n";
		
		// Just exicute the first step
		Integer currentStep = stepNumbers.get(0);
		Object tut = null;
		try {
			tut = tutorialClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			ioManager.clearScreen();
			ioManager.writeLine("Could not create tutorial object.");
			ioManager.writeLine(e.getMessage());
			ioManager.writeLine(e.getStackTrace().toString());
			ioManager.readInput("Press enter to continue:");
			return;
		}
		MainLoop: while (true) {
			TutorialStepReturnValue reslt = null;
			try {
				ioManager.clearScreen();
				reslt = (TutorialStepReturnValue)stepMethods.get(currentStep).invoke(tut, ioManager);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				ioManager.clearScreen();
				ioManager.writeLine("Could not exicute tutorial step.");
				ioManager.writeLine(e.getMessage());
				ioManager.writeLine(e.getStackTrace().toString());
				ioManager.readInput("Press enter to continue:");
				return;
			}		
			switch (reslt) {
			case EXIT:
				break MainLoop;
			case  NEXT_STEP:
				currentStep = currentStep.intValue() + 1;
				if(stepNumbers.indexOf(currentStep) == -1) {
					// Tutorial is finished, go back to tutorial menu
					break MainLoop;
				}
				break;
			case MENU:
				ioManager.clearScreen();
				ioManager.writeLine(prompt);
				InputLoop: while(true) {
				InputCommand cmd = ioManager.readInput("Make a choice:");
				if(cmd == InputCommand.EXIT) {
					break MainLoop;
				}
				Integer val = null;
				try {
					val = Integer.parseInt(ioManager.getLastInputString());
					} catch (Exception e) {
						ioManager.writeLine("Please select an option from the list above.");
						continue;
					}
				if(stepNumbers.indexOf(val) != -1) {
					currentStep = val;
					break InputLoop;
				}
				if(val.intValue() == 0) {
					break MainLoop;
				}
				ioManager.writeLine("Please select an option from the list above.");
				}
				break;
			case REPEAT:
				// Do nothing, since we want to run the current step again
				break;
			default:
				ioManager.writeLine("This should never exicute");
				ioManager.writeLine(reslt.toString());
				ioManager.readInput("Press enter to continue:");
				break;
			}
		}
	}

	private void playMusic() {
		String prompt = "Enter music strings to play music.  Type 'exit' to quit.";
		ioManager.clearScreen();
				ioManager.writeLine(prompt);
				ioManager.writeLine("\n");
		

				RealtimePlayer player;
				try {
					player = new RealtimePlayer();
				} catch (MidiUnavailableException e) {
					ioManager.writeLine("Could not start realtime player.");
					ioManager.writeLine(e.getMessage());
					return;
				}
				MusicParser parser = new MusicParser();
				String input = null;
while(true) {
InputCommand command = ioManager.readInput();

if(command == InputCommand.EXIT || command == InputCommand.MENU) { break; }
else if(command == InputCommand.HELP) { ioManager.writeLine(prompt); }

input = ioManager.getLastInputString();
try {
parser.ParseString(input);
player.play(parser.GetParsedPattern());
} catch(ParserException e) {
	ioManager.writeLine("Invalid music string.");
	ioManager.writeLine("");
}
}
player.close();
	}
	
}
