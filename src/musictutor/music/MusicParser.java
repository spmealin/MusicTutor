package musictutor.music;

import java.util.LinkedList;
import java.util.List;

import org.jfugue.parser.ParserException;
import org.jfugue.parser.ParserListenerAdapter;
import org.jfugue.pattern.Pattern;
import org.jfugue.pattern.PatternProducer;
import org.jfugue.theory.Chord;
import org.jfugue.theory.Note;
import org.staccato.StaccatoParser;

public class MusicParser extends ParserListenerAdapter {
	public static final byte REST_VALUE = -1;
	public static final byte CHORD_VALUE = -2;
	
private StaccatoParser parser;
private Pattern pattern;
private List<PatternProducer> patternComponents;
private int elementCount;
private int noteCount;
private int chordCount;
	
public MusicParser() {
	parser = new StaccatoParser();
	parser.setThrowsExceptionOnUnknownToken(true);
	parser.addParserListener(this);
}

public void ParseString(String s) throws ParserException {
	pattern = new Pattern();
	patternComponents = new LinkedList<>();
	elementCount = 0;
	noteCount = 0;
	chordCount = 0;
	try {
	parser.parse(s);
	} catch(ParserException ex) {
		pattern = null;
		patternComponents = null;
		elementCount = 0;
		noteCount = 0;
		chordCount = 0;
		throw ex;
	}
}

public Pattern GetParsedPattern() {
	assert pattern != null;
	return pattern;
}

public int GetParsedElementCount() {
	assert pattern != null;
	return elementCount;
}

public int GetParsedNoteCount() {
	assert pattern != null;
	return noteCount;
}

public int GetParsedChordCount() {
	assert pattern != null;
	return chordCount;
}

public List<PatternProducer> GetParsedComponents() {
	assert pattern != null;
	return patternComponents;
}

public String GetParsedCanonicalString() {
	assert pattern != null;
	StringBuilder sb = new StringBuilder();
	for(PatternProducer p : patternComponents) {
		if(p instanceof Note) {
			Note n = (Note)p;
			sb.append(n.getToneString());
			sb.append(n.getOctave());
			sb.append(Note.getDurationString(n.getDuration()));
			sb.append(" ");
		}
	}
	return sb.toString().trim();
}

public String[] GetParsedCanonicalStringAsArray() {
	return GetParsedCanonicalString().split(" ");
}

public String GetParsedNotesString() {
	assert pattern != null;
	StringBuilder sb = new StringBuilder();
	for(PatternProducer p : patternComponents) {
		if(p instanceof Note) {
			Note n = (Note)p;
			sb.append(n.getToneString());
			sb.append(" ");
		}
	}
	return sb.toString().trim();
}

public String[] GetParsedNotesStringAsArray() {
	return GetParsedNotesString().split(" ");
}

public byte[] GetParsedValues() {
	assert pattern != null;
	byte[] values = new byte[patternComponents.size()];
	int count = 0;
	for(PatternProducer p : patternComponents) {
		if(p instanceof Note) {
			if(((Note)p).isRest()) {
				values[count] = REST_VALUE;
			} else {
			values[count] = ((Note)p).getValue();
			}
		} else {
			values[count] = CHORD_VALUE;
		}
		count++;
	}
	return values;
}

@Override
public void onNoteParsed(Note note) {
pattern.add(note);
patternComponents.add(note);
elementCount++;
noteCount++;
}

@Override
public void onChordParsed(Chord chord) {
pattern.add(chord);
patternComponents.add(chord);
elementCount++;
chordCount++;
}

}
