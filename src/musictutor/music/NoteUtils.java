package musictutor.music;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.jfugue.theory.Note;

public final class NoteUtils {
	public static final Set<String> NOTES;
	public static final Set<String> NATURAL_NOTES;
	public static final Set<String> SHARP_NOTES;
	public static final Set<String> FLAT_NOTES;
	public static final List<Set<String>> ENHARMONIC_NOTES;

	public static final int NUMBER_OF_NOTES = 12;
	public static final int NUMBER_OF_NATURAL_NOTES = 7;
	public static final int NUMBER_OF_SHARP_NOTES = 5;
	public static final int NUMBER_OF_FLAT_NOTES = 5;
	public static final int NUMBER_OF_AUGMENTED_NOTES = 10;
	
	private static final Random RANDOM = new Random();
	
	static {
		Set<String> tempSet;
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("cb", "c", "c#", "db", "d", "d#", "eb", "e", "e#", "fb", "f", "f#", "gb", "g", "g#", "ab", "a", "a#", "bb", "b", "b#"));
		NOTES = Collections.unmodifiableSet(tempSet);
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("c", "d", "e", "f", "g", "a", "b"));
		NATURAL_NOTES = Collections.unmodifiableSet(tempSet);
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("c#", "d#", "e#", "f#", "g#", "a#", "b#"));
		SHARP_NOTES = Collections.unmodifiableSet(tempSet);
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("cb", "db", "eb", "fb", "gb", "ab", "bb"));
		FLAT_NOTES = Collections.unmodifiableSet(tempSet);
		
		List<Set<String>> tempArray = new ArrayList<>();
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("b#", "c"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("c#", "db"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("d"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("d#", "eb"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("e", "fb"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("e#", "f"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("f#", "gb"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("g"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("g#", "ab"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("a"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("a#", "bb"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		tempSet = new HashSet<>();
		tempSet.addAll(Arrays.asList("b", "cb"));
		tempArray.add(Collections.unmodifiableSet(tempSet));
		
		ENHARMONIC_NOTES = Collections.unmodifiableList(tempArray);
	}
	
	private NoteUtils() {
		// This constructor does nothing
		// Used to mark this class as a static class
	}
	
	public static Set<String> GetEnharmonicNotes(String note) {
		for(Set<String> s : ENHARMONIC_NOTES) {
			if(s.contains(note)) {
				return s;
			}
		}
		return null;
	}
	
	public static String GetCanonicalNote(Set<String> notes) {
		String[] names = notes.toArray(new String[0]);
		String flat = null;
		String sharp = null;
		for(String s : names) {
			if(s.length() == 1) { return s; }
			else if(s.endsWith("#")) { sharp = s; }
			else if(s.endsWith("b")) { flat = s; }
		}
		if(sharp != null) {
			return sharp;
		} else {
			return flat;
		}
	}
	
	public static String GetCanonicalNote(byte note) {
		// Just prefer either natural or sharp notes
		return GetNoteNamePreferSharp(note);
	}
	
	public static String GetRandomEnharmonicNote(Set<String> notes) {
		int choice = RANDOM.nextInt(notes.size());
		String[] names = notes.toArray(new String[0]);
		return names[choice];
	}
	
	public static String GetRandomEnharmonicNote(String note) {
		return GetRandomEnharmonicNote(GetEnharmonicNotes(note));
	}
	
	public static String GetNoteName(byte note) {
		if(RANDOM.nextBoolean()) {
			return GetNoteNamePreferSharp(note);
		} else {
			return GetNoteNamePreferFlat(note);
		}
	}
	
	public static String GetNoteNamePreferSharp(byte note) {
		return Note.getDispositionedToneStringWithoutOctave(1, note);
	}
	
	public static String GetNoteNamePreferFlat(byte note) {
		return Note.getDispositionedToneStringWithoutOctave(-1, note);
	}
	
	public static boolean IsNaturalNote(String note) {
		return NATURAL_NOTES.contains(note);
	}
	
	public static boolean IsNaturalNote(byte note) {
		int value = note % NUMBER_OF_NOTES;
		return value == 0 || value == 2 || value == 4 || value == 5 || value == 7 || value == 9 || value == 11;
	}
	
	public static boolean IsSharpNote(String note) {
		return SHARP_NOTES.contains(note);
	}
	
	public static boolean IsSharpNote(byte note) { 
		int value = note % NUMBER_OF_NOTES;
		return value == 1 || value == 3 || value == 6 || value == 8 || value == 10 || value == 9 || value == 11;
	}
	
	public static boolean IsFlatNote(String note) {
		return FLAT_NOTES.contains(note);
	}
	
	public static boolean IsFlatNote(byte note) {
		int value = note % NUMBER_OF_NOTES;
		return value == 1 || value == 3 || value == 6 || value == 8 || value == 10 || value == 9 || value == 11;
	}
	
	public static boolean IsEnharmonic(String note1, String note2) {
		return GetEnharmonicNotes(note1).contains(note2);
	}
}
