package musictutor.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputOutputManager {
	public final String COMMAND_MENU = "menu";
	public final String COMMAND_HELP = "help";
	public final String COMMAND_EXIT = "exit";
	
	private BufferedReader consoleReader;
	private String lastInputString;
	
	public InputOutputManager() {
		consoleReader = new BufferedReader(new InputStreamReader(System.in));
		lastInputString = null;
	}

	public InputCommand readInput() {
		return readInput(null);
	}
	
	public InputCommand readInput(String prompt) {
		if(prompt != null) {
		write(prompt + " ");
		}
		String temp = readConsoleLine().trim().toLowerCase();
		String tempTokens[] = temp.split(" ");
		lastInputString = String.join(" ", tempTokens);
		
		switch (lastInputString) {
		case COMMAND_MENU:
			return InputCommand.MENU;
		case COMMAND_HELP:
			return InputCommand.HELP;
		case COMMAND_EXIT:
			return InputCommand.EXIT;
		default:
			return InputCommand.OTHER;
		}
	}
	
	public String getLastInputString() {
		return lastInputString;
	}
	
	public void clearScreen() {
		try {
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		} catch (InterruptedException | IOException e) {
			// Do nothing
		}	
	}
	
	public void write(String str) {
		System.out.print(str);
	}
	
	public void writeLine(String str) {
		System.out.println(str);
	}
	
	private String readConsoleLine() {
		String input = "";
		try {
			input = consoleReader.readLine();
		} catch (IOException e) {
			// Do nothing
		}
		return input;
	}
}
