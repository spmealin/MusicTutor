package musictutor.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.atteo.classindex.IndexAnnotated;

@Retention(RetentionPolicy.RUNTIME)
@IndexAnnotated
@Target(ElementType.METHOD)
public @interface TutorialStep {
String title();
int stepNumber();
}
